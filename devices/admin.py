from django.contrib import admin
from devices.models.devices import DeviceModel
from devices.models.sensors import SensorModel
from devices.models.device_sensors import DeviceSensorsModel
from devices.models.device_sensor_data import DeviceSensorsDataModel

class DeviceAdmin(admin.ModelAdmin):
    list_display = [
        'id', 'name', 'model', 'created_at', 'updated_at', 'is_archived'
    ]

    ordering = ['-created_at' ]

    search_fields = [
        'name', 'model'
    ]

    list_filter = ['is_archived']


class SensorAdmin(admin.ModelAdmin):
    list_display = [
        'id', 'name', 'model', 'type', 'created_at', 'updated_at', 'is_archived'
    ]

    ordering = ['-created_at']

    search_fields = [
        'name', 'model'
    ]

    list_filter = ['type', 'is_archived']

class DeviceSensorsAdmin(admin.ModelAdmin):
    list_display = [
        'id', 'device', 'sensor', 'created_at', 'updated_at'
    ]

    ordering = ['-created_at']


class DeviceSensorsDataAdmin(admin.ModelAdmin):
    list_display = [
        'id', 'device_sensor', 'created_at', 'updated_at', 'is_archived'
    ]

    ordering = ['-created_at' ]

    list_filter = ['is_archived']

admin.site.register(DeviceModel, DeviceAdmin)
admin.site.register(SensorModel, SensorAdmin)
admin.site.register(DeviceSensorsModel, DeviceSensorsAdmin)
admin.site.register(DeviceSensorsDataModel, DeviceSensorsDataAdmin)