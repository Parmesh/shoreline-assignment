from rest_framework import viewsets

from devices.models import DeviceSensorsDataModel
from devices.serializers import DeviceSensorsDataSerializer
from main_project.utility import update_request_data_on_object_creation


class DeviceSensorsDataViewSet(viewsets.ModelViewSet):
    queryset = DeviceSensorsDataModel.objects.filter(
        is_archived=False
    )
    serializer_class = DeviceSensorsDataSerializer

    # Adding ordering capabilities
    ordering_fields = [
        'created_at', 'updated_at'
    ]

    def get_queryset(self):
        # In case of listing retrieve only users organization device_sensor related data
        return self.queryset.filter(
            device_sensor__device__organization_id=self.request.user.owner
        )

    def create(self, request, *args, **kwargs):
        """
        To add sensor data.

        ## Objective
            To add sensor data.

        ## API Capabilities
            1. Authentication - Required
            2. Paging supported - NA
            3. Ordering fields - NA
            4. Searchable fields - NA
            5. Filterable fields - NA
            6. Expandable fields- NA

        ## Input Validations
            1. Check if requested user have a valid access token.
            2. Check if request contains valid data along with its type

        ## Business Cases
            1. API will store sensor data in device sensor data model.
            4. API will return newly stored sensor data.


        ## Error Codes
            401 - Unauthorized Access
            400 - Bad request
            403 - Forbidden

        ## Request Body
            {
              "data": "{'temp': 50}"
            }

        ## Response Body
            {
                "id": "d5f5cf40-2d7b-4196-b0f7-847eb8310cc1",
                "device_sensor": "1f2ab7fc-43ce-4bbf-9aa2-b7b79738aaca",
                "data": "{'temp': 50}",
                "created_at": "2022-01-15T20:26:53.279642+05:30",
                "updated_at": "2022-01-15T20:26:53.279828+05:30"
            }
        """
        context = {'view': self, 'request': request}
        request.data["device_sensor"] = request.parser_context['kwargs']['pk']
        update_request_data_on_object_creation(request.data, request)
        serializer = self.get_serializer(data=request.data, context=context)
        serializer.is_valid(raise_exception=True)
        return super(DeviceSensorsDataViewSet, self).create(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        """
        To retrieve list of sensor data

        ## Objective
            To retrieve list of sensor data

        ## API Capabilities
            1. Authentication - Required
            2. Paging supported - Yes
            3. Ordering fields - [created_at, updated_at]
            4. Searchable fields - [data]
            5. Filterable fields - NA
            6. Expandable fields- NA

        ## Input Validations
            1. Check if requested user have a valid access token.

        ## Business Cases
            1. Filter DeviceSensorDataModel queryset with device sensor id
            2. Retrieve list of data associated with sensor.

        ## Error Codes
            401- Unauthorized access
            403 - Forbidden

        ## Response Body
            {
                "count": 1,
                "next": null,
                "previous": null,
                "results": [
                    {
                        "id": "d5f5cf40-2d7b-4196-b0f7-847eb8310cc1",
                        "device_sensor": "1f2ab7fc-43ce-4bbf-9aa2-b7b79738aaca",
                        "data": "{'temp': 50}",
                        "created_at": "2022-01-15T20:26:53.279642+05:30",
                        "updated_at": "2022-01-15T20:26:53.279828+05:30"
                    }
                ]
            }
        """
        return super(DeviceSensorsDataViewSet, self).list(request, *args, **kwargs)
