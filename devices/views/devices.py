from rest_framework import viewsets

from devices.models import DeviceModel
from devices.serializers import DevicesListSerializer, DeviceSerializer
from main_project.utility import update_request_data_on_object_creation, update_request_data_on_object_updation


class DevicesViewSet(viewsets.ModelViewSet):
    queryset = DeviceModel.objects.filter(
        is_archived=False
    )
    serializer_class = DeviceSerializer

    # Adding ordering, search capabilities
    ordering_fields = [
        'created_at', 'updated_at'
    ]
    search_fields = [
        'name', 'model'
    ]

    def get_queryset(self):
        # In case of listing retrieve only users organization devices
        return self.queryset.filter(
            organization_id=self.request.user.owner
        )

    def get_serializer_class(self):
        if self.action == 'list':
            return DevicesListSerializer
        return self.serializer_class

    def create(self, request, *args, **kwargs):
        """
        To add a new device in organization.

        ## Objective
            To add a new device in organization.

        ## API Capabilities
            1. Authentication - Required
            2. Paging supported - NA
            3. Ordering fields - NA
            4. Searchable fields - NA
            5. Filterable fields - NA
            6. Expandable fields- NA

        ## Input Validations
            1. Check if requested user have a valid access token.
            2. Check if request contains valid data along with its type

        ## Business Cases
            1. API will create a new device with given information.
            2. Newly created device will be associated with organization
               of requested user.
            3. API will add temperature and pressure sensor for newly created device
            4. API will return created device details.


        ## Error Codes
            401 - Unauthorized Access
            400 - Bad request
            403 - Forbidden

        ## Request Body
            {
              "name": "Washing Machine",
              "model": "1232",
              "weight": "10 KG",
              "firmware_version": "1.0.1"
            }

        ## Response Body
            {
                "id": "5e2c5469-c0a7-4b95-a2c5-9ab4f1458a1a",
                "name": "Laptop",
                "model": "NAHs",
                "weight": "0.25 KG",
                "firmware_version": "1.0.1",
                "organization": "ccfd2c2e-2ea8-4c1b-9a22-c8e8a479274b",
                "created_at": "2022-01-15T19:48:44.505104+05:30",
                "updated_at": "2022-01-15T19:48:44.505212+05:30",
                "device_sensors": [
                    {
                        "id": "0faad93c-49c2-4a62-bdcd-fe15e554a0da",
                        "sensor": {
                            "id": "72770882-d962-4c66-9de8-956e6355c3df",
                            "name": "Temperature Sensor"
                        }
                    },
                    {
                        "id": "96ae9ee9-0c6d-421b-a734-dd9418161d4b",
                        "sensor": {
                            "id": "f8bda74a-143a-495a-96ff-f7672891c084",
                            "name": "Pressure Sensor"
                        }
                    }
                ]
            }
        """
        context = {'view': self, 'request': request}
        request.data["organization"] = request.parser_context['kwargs']['pk']
        update_request_data_on_object_creation(request.data, request)
        serializer = self.get_serializer(data=request.data, context=context)
        serializer.is_valid(raise_exception=True)
        return super(DevicesViewSet, self).create(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        """
        To retrieve list devices of requested users organization

        ## Objective
            To retrieve list devices of requested users organization

        ## API Capabilities
            1. Authentication - Required
            2. Paging supported - Yes
            3. Ordering fields - [created_at, updated_at]
            4. Searchable fields - [name, model]
            5. Filterable fields - NA
            6. Expandable fields- NA

        ## Input Validations
            1. Check if requested user have a valid access token.

        ## Business Cases
            1. Filter Devices model queryset with users organization
            2. Retrieve list of devices associated with the users organization.

        ## Error Codes
            401- Unauthorized access
            403 - Forbidden

        ## Response Body
            {
                "count": 2,
                "next": null,
                "previous": null,
                "results": [
                    {
                        "id": "16e43aaf-4a28-489e-9e5c-f746fd662f80",
                        "name": "Washing Machine",
                        "model": "1232",
                        "weight": "10 KG",
                        "firmware_version": "1.0.1",
                        "organization": "ccfd2c2e-2ea8-4c1b-9a22-c8e8a479274b",
                        "created_at": "2022-01-14T11:42:20.178055+05:30",
                        "updated_at": "2022-01-14T11:42:20.178190+05:30"
                    },
                    {
                        "id": "51282fa6-fa5a-424e-b946-fdaf9034cfe2",
                        "name": "Mobile",
                        "model": "JHJSDIS",
                        "weight": "0.2 KG",
                        "firmware_version": "1.0.1",
                        "organization": "ccfd2c2e-2ea8-4c1b-9a22-c8e8a479274b",
                        "created_at": "2022-01-14T11:57:09.403490+05:30",
                        "updated_at": "2022-01-14T11:57:09.403593+05:30"
                    }
                ]
            }
        """
        return super(DevicesViewSet, self).list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        """
        To retrieve device details

        ## Objective
            To retrieve device details

        ## API Capabilities
            1. Authentication - Required
            2. Paging supported - NA
            3. Ordering fields - NA
            4. Searchable fields - NA
            5. Filterable fields - NA
            6. Expandable fields- NA

        ## Input Validations
            1. Check if requested user have a valid access token.
            2. Check if device belongs to users organization

        ## Business Cases
            1. Filter Devices model queryset with users organization
            2. Retrieve device details.

        ## Error Codes
            401- Unauthorized access
            403 - Forbidden

        ## Response Body
            {
                "id": "5e2c5469-c0a7-4b95-a2c5-9ab4f1458a1a",
                "name": "Laptop",
                "model": "NAHs",
                "weight": "0.25 KG",
                "firmware_version": "1.0.1",
                "organization": "ccfd2c2e-2ea8-4c1b-9a22-c8e8a479274b",
                "created_at": "2022-01-15T19:48:44.505104+05:30",
                "updated_at": "2022-01-15T19:48:44.505212+05:30",
                "device_sensors": [
                    {
                        "id": "0faad93c-49c2-4a62-bdcd-fe15e554a0da",
                        "sensor": {
                            "id": "72770882-d962-4c66-9de8-956e6355c3df",
                            "name": "Temperature Sensor"
                        }
                    },
                    {
                        "id": "96ae9ee9-0c6d-421b-a734-dd9418161d4b",
                        "sensor": {
                            "id": "f8bda74a-143a-495a-96ff-f7672891c084",
                            "name": "Pressure Sensor"
                        }
                    }
                ]
            }
        """
        return super(DevicesViewSet, self).retrieve(request, *args, **kwargs)

    def partial_update(self, request, *args, **kwargs):
        """
        To update device details

        ## Objective
            To update device details

        ## API Capabilities
            1. Authentication - Required
            2. Paging supported - NA
            3. Ordering fields - NA
            4. Searchable fields - NA
            5. Filterable fields - NA
            6. Expandable fields- NA

        ## Input Validations
            1. Check if requested user have a valid access token.
            2. Check if device belongs to users organization

        ## Business Cases
            1. Filter Devices model queryset with users organization
            2. update device details in partial way.

        ## Error Codes
            401- Unauthorized access
            403 - Forbidden

        ## Request Data
            {
                "name": "Mobile",
                "model": "JHJSDIS"
            }

        ## Response Body
            {
                "id": "5e2c5469-c0a7-4b95-a2c5-9ab4f1458a1a",
                "name": "Laptop",
                "model": "NAHs",
                "weight": "0.25 KG",
                "firmware_version": "1.0.1",
                "organization": "ccfd2c2e-2ea8-4c1b-9a22-c8e8a479274b",
                "created_at": "2022-01-15T19:48:44.505104+05:30",
                "updated_at": "2022-01-15T19:48:44.505212+05:30",
                "device_sensors": [
                    {
                        "id": "0faad93c-49c2-4a62-bdcd-fe15e554a0da",
                        "sensor": {
                            "id": "72770882-d962-4c66-9de8-956e6355c3df",
                            "name": "Temperature Sensor"
                        }
                    },
                    {
                        "id": "96ae9ee9-0c6d-421b-a734-dd9418161d4b",
                        "sensor": {
                            "id": "f8bda74a-143a-495a-96ff-f7672891c084",
                            "name": "Pressure Sensor"
                        }
                    }
                ]
            }
        """
        update_request_data_on_object_updation(request.data, request)
        kwargs["partial"] = True
        return self.update(request, *args, **kwargs)
