from django.db import models

from users.models import OrganizationModel
from main_project.utility import BaseModel


SENSOR_TYPE = [
    ('temperature', 'Temperature'),
    ('pressure', 'Pressure'),
    ('other', 'Other')
]

class SensorModel(BaseModel):
    name = models.CharField(
        max_length=1024, unique=True
    )
    model = models.CharField(
        max_length=1024, null=True, blank=True
    )
    type = models.CharField(
        max_length=128, choices=SENSOR_TYPE, default='other'
    )
    is_archived = models.BooleanField(default=False)

    class Meta:
        db_table = 'SensorModel'
        verbose_name = 'Sensor'
        verbose_name_plural = 'Sensors'

    def __str__(self):
        return self.name  # pragma: no cover
