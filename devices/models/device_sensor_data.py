from django.db import models
from jsonfield import JSONField

from devices.models.device_sensors import DeviceSensorsModel
from main_project.utility import BaseModel


class DeviceSensorsDataModel(BaseModel):
    device_sensor = models.ForeignKey(
        DeviceSensorsModel, on_delete=models.PROTECT,
    )
    data = JSONField(null=True, blank=True)
    is_archived = models.BooleanField(default=False)

    class Meta:
        db_table = 'DeviceSensorsDataModel'
        verbose_name = 'Device Sensor Data'
        verbose_name_plural = 'Device Sensors Data'

    def __str__(self):
        return str(self.device_sensor.device.name + '-' + self.device_sensor.sensor.name)
