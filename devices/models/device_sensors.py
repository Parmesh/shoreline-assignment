from django.db import models

from devices.models import DeviceModel, SensorModel
from main_project.utility import BaseModel


class DeviceSensorsModel(BaseModel):
    device = models.ForeignKey(
        DeviceModel, on_delete=models.PROTECT,
        related_name='device_sensors'
    )
    sensor = models.ForeignKey(
        SensorModel, on_delete=models.PROTECT
    )

    class Meta:
        db_table = 'DeviceSensorsModel'
        verbose_name = 'Device Sensor'
        verbose_name_plural = 'Device Sensors'

    def __str__(self):
        return str(self.device.name + '-' + self.sensor.name)
