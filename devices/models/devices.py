from django.db import models

from main_project.utility import BaseModel
from users.models import OrganizationModel


class DeviceModel(BaseModel):
    name = models.CharField(
        max_length=1024, unique=True
    )
    model = models.CharField(
        max_length=1024, null=True, blank=True
    )
    weight = models.CharField(
        max_length=1024, null=True, blank=True
    )
    firmware_version = models.CharField(
        max_length=1024, null=True, blank=True
    )
    organization = models.ForeignKey(
        OrganizationModel, on_delete=models.PROTECT,
        related_name='device_organization'
    )
    is_archived = models.BooleanField(default=False)

    class Meta:
        db_table = 'DeviceModel'
        verbose_name = 'Device'
        verbose_name_plural = 'Devices'

    def __str__(self):
        return str(self.name)  # pragma: no cover
