from django.urls import re_path

from devices.views.device_sensors_data import DeviceSensorsDataViewSet
from devices.views.devices import DevicesViewSet

urlpatterns = [
    re_path(
        r'^organization/(?P<pk>[0-9a-f-]+)/devices/?$',
        DevicesViewSet.as_view({
            'post': 'create',
            'get': 'list'
        }),
        name='organization_devices_create_list_view'
    ),
    re_path(
        r'^devices/(?P<pk>[0-9a-f-]+)/?$',
        DevicesViewSet.as_view({
            'patch': 'partial_update',
            'get': 'retrieve'
        }),
        name='devices_update_retrieve_view'
    ),
    re_path(
        r'^device_sensors/(?P<pk>[0-9a-f-]+)/data/?$',
        DeviceSensorsDataViewSet.as_view({
            'post': 'create',
            'get': 'list'
        }),
        name='devices_sensors_data_create_list_view'
    ),
]
