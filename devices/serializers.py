from rest_framework import serializers

from devices.models import DeviceModel, SensorModel, DeviceSensorsDataModel
from devices.models.device_sensors import DeviceSensorsModel


class DevicesListSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeviceModel
        fields = [
            'id', 'name', 'model', 'weight', 'firmware_version', 'organization',
            'created_at', 'updated_at'
        ]


class DeviceSerializer(serializers.ModelSerializer):
    device_sensors = serializers.SerializerMethodField()

    def get_device_sensors(self, instance):
        return DeviceSensorsSerializer(instance.device_sensors.all() ,many=True).data

    class Meta:
        model = DeviceModel
        fields = [
            'id', 'name', 'model', 'weight', 'firmware_version', 'organization',
            'created_at', 'updated_at', 'device_sensors'
        ]

class DeviceSensorsSerializer(serializers.ModelSerializer):
    sensor = serializers.SerializerMethodField()

    def get_sensor(self, instance):
        return SensorSerializer(instance=instance.sensor).data

    class Meta:
        model = DeviceSensorsModel
        fields = [
            'id', 'sensor'
        ]


class SensorSerializer(serializers.ModelSerializer):
    class Meta:
        model = SensorModel
        fields = [
            'id', 'name'
        ]


class DeviceSensorsDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeviceSensorsDataModel
        fields = [
            'id', 'device_sensor', 'data', 'created_at', 'updated_at'
        ]