from django.db.models.signals import post_save
from django.dispatch import receiver

from devices.models import DeviceModel
from devices.models.device_sensors import DeviceSensorsModel
from devices.models.sensors import SENSOR_TYPE
from devices.models.sensors import SensorModel


@receiver(post_save, sender=DeviceModel)
def create_or_update_device(sender, instance, created, **kwargs):
    # Adding temperature and pressure sensors to device
    if created:
        temperature_sensor = SensorModel.objects.filter(
            type=SENSOR_TYPE[0][0]
        ).first()
        DeviceSensorsModel.objects.create(
            device=instance,
            sensor=temperature_sensor,
            created_by=instance.created_by,
            last_modified_by=instance.last_modified_by
        )
        pressure_sensor = SensorModel.objects.filter(
            type=SENSOR_TYPE[1][0]
        ).first()
        DeviceSensorsModel.objects.create(
            device=instance,
            sensor=pressure_sensor,
            created_by=instance.created_by,
            last_modified_by=instance.last_modified_by
        )
    else:
        pass
