from django.db import models

from main_project.utility import BaseModel


class JwtSession(BaseModel):
    """ JWT User sessions """
    access_token = models.CharField(max_length=2000)
    refresh_token = models.CharField(max_length=2000)
    access_token_expiry = models.DateTimeField()
    refresh_token_expiry = models.DateTimeField()
    device_ip = models.CharField(max_length=128, null=True, blank=True)

    class Meta:
        db_table = 'jwt_session'
        verbose_name = 'JWT Sessions'
        verbose_name_plural = 'JWT Sessions'
