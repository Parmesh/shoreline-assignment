import uuid

from django.db import models


class OrganizationModel(models.Model):
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False
    )
    name = models.CharField(max_length=1024)
    organization_id = models.CharField(max_length=14)
    user = models.UUIDField(null=True, blank=True)
    is_archived = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'OrganizationModel'
        verbose_name = 'Organization'
        verbose_name_plural = 'Organizations'

    def __str__(self):
        return str(self.name)  # pragma: no cover
