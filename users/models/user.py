import uuid

from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models


class User(AbstractUser):
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False
    )
    owner = models.UUIDField(null=True, blank=True)
    objects = UserManager()

    class Meta:
        app_label = 'users'
        db_table = 'Users'
