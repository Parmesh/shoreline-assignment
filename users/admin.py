from django.contrib import admin
from users.models.user import User
from users.models.organization import OrganizationModel

class UserAdmin(admin.ModelAdmin):
    list_display = [
        'email', 'first_name', 'last_name', 'is_active',
    ]

    ordering = ['-date_joined', ]

    search_fields = [
        'first_name', 'last_name', 'email'
    ]

    list_filter = ['is_active']


class OrganizationAdmin(admin.ModelAdmin):
    list_display = [
        'organization_id', 'name', 'is_active', 'created_at', 'updated_at'
    ]

    ordering = ['-created_at', 'updated_at']

    search_fields = [
        'name',
    ]

    list_filter = ['is_active']

admin.site.register(User, UserAdmin)
admin.site.register(OrganizationModel, OrganizationAdmin)