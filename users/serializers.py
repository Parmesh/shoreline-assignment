import logging
import random
import string

from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import transaction
from django.db.models import Q
from django.utils import timezone
from django.utils.encoding import smart_text
from rest_framework import exceptions, serializers, status
from rest_framework_simplejwt import serializers as jwt_serializers
from rest_framework_simplejwt.tokens import RefreshToken

from main_project.utility import CustomAPIException
from users.models import JwtSession, OrganizationModel

User = get_user_model()
logger = logging.getLogger(__name__)


class RegisterUserSerializer(serializers.ModelSerializer):
    # validations for required field to create user account
    first_name = serializers.CharField(max_length=128)
    last_name = serializers.CharField(max_length=128)
    email = serializers.EmailField()
    username = serializers.CharField(required=False)
    password = serializers.CharField(write_only=True)

    def validate_email(self, email):
        # Validating if account with same email id exist
        if User.objects.filter(email__iexact=email).exists():
            logger.error(
                'A new user trying to register with email already exists.')
            raise CustomAPIException(
                status_code=status.HTTP_409_CONFLICT,
                message='User with same email already exist',
                error_code='1001'
            )
        return email.lower()

    def validate(self, attrs):
        # Adding email as username as it is required to create user in django
        attrs['username'] = attrs['email']
        return attrs

    def create(self, validated_data):

        try:
            # controlling database transactions
            with transaction.atomic():
                # Create a user object in Django User Model
                user = User.objects.create_user(**validated_data)
                # Create a Organization object in Organization Model
                data = {
                    'name': user.first_name + ' ' + user.last_name,
                    'user': str(user.id)
                }
                organization_serializer = OrganizationSerializer(data=data, context=self.context)
                organization_serializer.is_valid(raise_exception=True)
                organization = organization_serializer.save()

                # Storing organization id as a owner on user model to refer user organization wherever needed
                user.owner = organization.id
                user.save()
                return user
        except exceptions.APIException:
            raise CustomAPIException(
                status_code=status.HTTP_409_CONFLICT,
                message='Failed while registering user',
                error_code='1002'
            )

    class Meta:
        model = User
        fields = ('username', 'email', 'password', 'first_name', 'last_name',)


class UserBasicInfoSerializer(serializers.ModelSerializer):
    organization_id = serializers.SerializerMethodField()

    def get_organization_id(self, instance):
        return instance.owner


    class Meta:
        model = User
        fields = ('id', 'email', 'first_name', 'last_name', 'organization_id')


class LoginSerializer(serializers.Serializer):
    def authenticate_user_with_username_or_email(self):
        """
        Authenticate user using email and password
        :return: refresh and access token pair
        """
        auth_serializer = TokenObtainPairSerializer(data=self.initial_data,
                                                    context=self.context)
        auth_serializer.is_valid(raise_exception=True)
        return auth_serializer.validated_data


class TokenObtainSerializer(jwt_serializers.TokenObtainSerializer):
    def validate(self, attrs):
        self.user = User.objects.filter(Q(
            username__iexact=attrs[self.username_field]) | Q(
            email__iexact=attrs[self.username_field])).first()
        if not self.user or not self.user.check_password(attrs['password']):
            raise CustomAPIException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                message='No active account found for given credentials',
                error_code='1003'
            )
        return {}


class TokenObtainPairSerializer(TokenObtainSerializer):
    @classmethod
    def get_token(cls, user):
        return RefreshToken.for_user(user)

    @classmethod
    def store_jwt_session(cls, token_pair, user, request):
        """
        Store JWT token pair in user session
        :param token_pair: dict (access and refresh token pair)
        :param user:  Django user
        :param request: http request
        :return: None
        """
        session_instance = JwtSession(
            refresh_token=token_pair['refresh'],
            access_token=token_pair['access'],
            access_token_expiry=timezone.now() + settings.SIMPLE_JWT[
                'ACCESS_TOKEN_LIFETIME'],
            refresh_token_expiry=timezone.now() + settings.SIMPLE_JWT[
                'REFRESH_TOKEN_LIFETIME'],
            device_ip=request.META.get('REMOTE_ADDR')
        )
        session_instance.save()

    @classmethod
    def generate_token(cls, user, request):
        """
        Generate JWT Token pair for user
        :param user: Django user
        :param request: http request
        :return: dict (access and refresh token pair)
        """
        refresh = cls.get_token(user)
        token_pair = {
            'refresh': smart_text(refresh),
            'access': smart_text(refresh.access_token)
        }

        # store token pair in user sessions
        cls.store_jwt_session(token_pair, user, request)

        return token_pair

    def validate(self, attrs):
        super(TokenObtainPairSerializer, self).validate(attrs)
        return self.generate_token(user=self.user,
                                   request=self.context['request'])


# utility specific to generate organization id
def get_random_organization_id(company_name):
    name = (company_name.replace(" ", ''))[:5]
    account_id = name + '-' + ''.join(
        random.choices(string.ascii_lowercase + string.digits, k=5)
    )
    return account_id


class OrganizationSerializer(serializers.ModelSerializer):
    def validate(self, attrs):
        attrs['organization_id'] = get_random_organization_id(
            attrs['name'],
        )
        return attrs

    class Meta:
        model = OrganizationModel
        fields = '__all__'
        read_only_fields = ['organization_id']
