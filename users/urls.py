from django.urls import re_path

from users.views.register import RegisterView
from users.views.login import LoginView

urlpatterns = [
    re_path(
        r'^users/register/?$',
        RegisterView.as_view({
            'post': 'create'
        }),
        name='user_register'
    ),
    re_path(
        r'^users/login/?$',
        LoginView.as_view(),
        name='login_view'
    )
]
