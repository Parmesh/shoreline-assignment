from rest_framework import status, views
from rest_framework.response import Response

from users.serializers import LoginSerializer


class LoginView(views.APIView):
    authentication_classes = ()
    permission_classes = ()

    def post(self, request):
        """
        To login using email and password.

        ## Objective
            To login using email and password


        ## API Capabilities
            1. Authentication - NA
            2. Paging supported - NA
            3. Ordering fields - NA
            4. Searchable fields - NA
            5. Filterable fields - NA
            6. Expandable fields- NA

        ## Input Validations
            1. Check if request data contains email and password.
            2. Check if request data is of valid type.

        ## Business Cases
            1. Find user with given credentials.
            2. Generate Access & Refresh Token for User
            3. Return response with 200 status code.

        ## Error Codes
            400 - Bad request
            401 - Unauthenticated
            404 - Not found


        ## Request Body
            {
                "email": "parmesh@gmail.com",
                "password": "Corning@123",
            }


        ## Response Body
            {
                "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTY0OTg2NTI3NiwiaWF0IjoxNjQyMDg5Mjc2LCJqdGkiOiJlN2Y2MjY5YWYyN2Y0ODBjOTkyN2RhZGRjNmRlMzM1NCIsInVzZXJfaWQiOiJkOTNlYjAyNy05MDhkLTRkMDUtYWZiZC02OWRjM2EwZTFmMzgifQ.MJrbRP1VRyIsyFMM4W5FVHBMVgBBpuzqkNAT6hWBg04",
                "access": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjQ3MjczMjc2LCJpYXQiOjE2NDIwODkyNzYsImp0aSI6ImJmOGRhNzU1YThmMzQ4YzE4Y2FiYmM5M2NiNGVmNDM2IiwidXNlcl9pZCI6ImQ5M2ViMDI3LTkwOGQtNGQwNS1hZmJkLTY5ZGMzYTBlMWYzOCJ9.tJQv8XqsXUYFqGbrY6l3r6S-cXZ9koR8gCnnybZLwFY"
            }
        """
        context = {'view': self, 'request': request}
        request.data['username'] = request.data['email'] if 'email' in request.data else ''
        serializer = LoginSerializer(data=request.data, context=context)
        serializer.is_valid(raise_exception=True)
        token_pair = serializer.authenticate_user_with_username_or_email()
        return Response(token_pair, status=status.HTTP_200_OK)
