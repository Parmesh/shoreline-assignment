from rest_framework import status, viewsets
from rest_framework.response import Response

from users.serializers import (
    RegisterUserSerializer, TokenObtainPairSerializer,
    UserBasicInfoSerializer)


class RegisterView(viewsets.ModelViewSet):
    authentication_classes = ()
    permission_classes = ()
    serializer_class = RegisterUserSerializer

    def create(self, request, *args, **kwargs):
        """
        To register a user with a organization.

        ## Objective
            To register a user with a organization.

        ## API Capabilities
            1. Authentication - NA
            2. Paging supported - NA
            3. Ordering fields - NA
            4. Searchable fields - NA
            5. Filterable fields - NA
            6. Expandable fields- NA

        ## Input Validations
            1. Check if request data contain first_name, last_name, email and password.
            2. Check if request data is of valid data type.
            3. Check if request contains all the required fields.

        ## Business Cases
            1. User should be able to enter first_name, last_name, email and password
            2. email will be stored as username
            3. Organization will be created with concatenating first_name and last_name.
            4. Access Token will be generated which can be used further to access other apis.


        ## Error Codes
            400 - Bad request
            409 - Conflict (email already exists)


        ## Request Body
            {
                "first_name": "Parmesh",
                "last_name": "Bhande"
                "email": "parmesh@gmail.com",
                "password": "Corning@123",
            }

        ## Response Body
            {
                "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTY0OTg2NTI3NiwiaWF0IjoxNjQyMDg5Mjc2LCJqdGkiOiJlN2Y2MjY5YWYyN2Y0ODBjOTkyN2RhZGRjNmRlMzM1NCIsInVzZXJfaWQiOiJkOTNlYjAyNy05MDhkLTRkMDUtYWZiZC02OWRjM2EwZTFmMzgifQ.MJrbRP1VRyIsyFMM4W5FVHBMVgBBpuzqkNAT6hWBg04",
                "access": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjQ3MjczMjc2LCJpYXQiOjE2NDIwODkyNzYsImp0aSI6ImJmOGRhNzU1YThmMzQ4YzE4Y2FiYmM5M2NiNGVmNDM2IiwidXNlcl9pZCI6ImQ5M2ViMDI3LTkwOGQtNGQwNS1hZmJkLTY5ZGMzYTBlMWYzOCJ9.tJQv8XqsXUYFqGbrY6l3r6S-cXZ9koR8gCnnybZLwFY"
            }
        """
        # validating and creating a user object and organization
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        # Get user basic info
        data = UserBasicInfoSerializer(instance=user).data
        # generate access token pair for signed up user
        access_token_pair = TokenObtainPairSerializer.generate_token(
            user=serializer.instance, request=request)
        data.update(access_token_pair)
        return Response(data=data, status=status.HTTP_201_CREATED)
