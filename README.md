Shoreline Assignment API's - Devices and Sensors Service
=======================

This is a Django project which is consist of APIs regards Devices and Sensors.

What's Here
-----------

This project includes:

* main_project - This directory contains your Django project files.
* devices -  This django application contains API's to add, update, list, retrieve devices and sensors.
* users - This django application contains API's related with signup and login 
* manage.py - This python script is used to start your django web application
* requirements.txt - This file is used to install python dependencies needed by
  the django application

Getting Started
---------------

## Prerequisites

- Python >= 3.7
- Django version >= 2.1
- PostgreSQL version >= 11.0 ---> For production
- sqlite3

## Installation on macOS

### Setup and activate Virtual Environment

      - virtualenv venv
      - source venv/bin/activate

### Install requirements

    pip install -r requirements.txt

### Migration

    python manage.py migrate
    
### Loading Default Sensors
 
    python manage.py loaddata sensors.json
    
    
### Create Django Admin

    python manage.py createsuperuser --email <admin_email> --username <admin_username>


### Run Server

    python manage.py runserver



Application useful urls
-----------    
## Django Admin Dashboard
    http://localhost:8000/admin/
    https://2djz11xqx0.execute-api.ap-south-1.amazonaws.com/dev/
    credentials :
        username : admin@gmail.com
        password : Admin@123

## Swagger documentation
    http://localhost:8000/swagger/
    http://localhost:8000/redoc/
    
    https://2djz11xqx0.execute-api.ap-south-1.amazonaws.com/dev/swagger
    https://2djz11xqx0.execute-api.ap-south-1.amazonaws.com/dev/redoc



    
    
