from django.conf.urls import include, url
from django.contrib import admin
from django.urls import re_path

from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from devices.urls import urlpatterns as devices_apis
from users.urls import urlpatterns as users_apis

schema_view = get_schema_view(
    openapi.Info(
        title='ShorelineIOT API',
        default_version='v1',
        description='ShorelineIOT APIs - V1',
        terms_of_service='',
        contact=openapi.Contact(
            name='Parmesh Bhande',
            email='parmeshbhande@gmail.com'
        ),
    ),
    public=True,
    patterns=[
        url('', include(users_apis)),
        url('', include(devices_apis)),
    ]
)


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url('', include(devices_apis)),
    url('', include(users_apis)),
    re_path(
        r'^swagger(?P<format>\.json|\.yaml)$',
        schema_view.without_ui(
            cache_timeout=0
        ),
        name='schema-json'
    ),
    re_path(
        r'^swagger/$',
        schema_view.with_ui(
            'swagger',
            cache_timeout=0
        ),
        name='schema-swagger-ui'
    ),
    re_path(
        r'^redoc/$',
        schema_view.with_ui(
            'redoc',
            cache_timeout=0
        ),
        name='schema-redoc'
    )
]
