from .base import *

# Local Database Configurations
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'shoreline-assignment.db'),
    }
}

ENVIRONMENT = 'local'

