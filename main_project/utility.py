import uuid

from django.db import models
from rest_framework import exceptions


class BaseModel(models.Model):
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False
    )
    created_by = models.UUIDField(null=True, blank=True)
    last_modified_by = models.UUIDField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class CustomAPIException(exceptions.APIException):
    """
    Raise custom API Exception with HTTP status code, error message and
    Custom error code
    """

    def __init__(self, status_code, message=None, error_code=None):
        self.status_code = status_code
        self.detail = {'detail': message, 'code': error_code}


def update_request_data_on_object_creation(dictionary, request):
    dictionary['created_by'] = str(request.user.id)
    dictionary['last_modified_by'] = str(request.user.id)
    dictionary['owner'] = str(request.user.owner)
    return dictionary


def update_request_data_on_object_updation(dictionary, request):
    dictionary['last_modified_by'] = str(request.user.id)
    return dictionary


